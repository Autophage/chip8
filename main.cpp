#include <cstdlib>
#include <cstdio>
#include <ncurses.h>
// #include   // OpenGL graphics and input
#include "chip8.h" // Your cpu core implementation
 
chip8 myChip8;
 
int main(int argc, char **argv) 
{
  // Set up render system and register input callbacks
  // setupGraphics();
  // setupInput();
 
  // initscr();
  // cbreak();
  // noecho();
  // keypad(stdscr, TRUE);

  // WINDOW* win = newwin(32, 64, 0, 0);

  // Initialize the Chip8 system and load the game into the memory  
  myChip8.initialize();
  myChip8.loadGame(argv[1]);
  // myChip8.loadGame("pong");
 
  // Emulation loop
  for(;;)
  {
    // Emulate one cycle
    myChip8.emulateCycle();
 
    // If the draw flag is set, update the screen
    // if(myChip8.drawFlag) {
      // wrefresh(win);
      // drawGraphics();
    // }
 
    // Store key press state (Press and Release)
    // myChip8.setKeys();	
  }
 
  // delwin(win);
  // endwin();
  return 0;
}
