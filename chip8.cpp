#include <iostream>
#include <iostream>
#include <unistd.h>
#include "chip8.h"

void chip8::initialize() {
  // Initialize program counter to start of program
  pc = 0x200;
  i = 0;
  sp = 0;
  missCount = 0;

  // Initialize registers and stack to 0
  for (char j = 0; j < 0xF; j++) {
    v[j] = 0;
    stack[j] = 0;
    key[j] = false;
  }

  // Load 0-F font into memory
  for (int j = 0; j < 80; j++) {
    mem[0x050 + j] = fonts[j];
  }

  // Clear the graphics buffer
  for (int j = 0; j < 32; j++) {
    for (int k = 0; k < 64; k++) {
      gfx[k][j] = ' ';
    }
  }

  delay_timer = 0;
  sound_timer = 0;

  renderFlag = false;

  initscr();
  cbreak();
  noecho();
  keypad(stdscr, TRUE);

  win = newwin(32, 64, 0, 0);
  debug = newwin(20, 64, 32, 0);
  scrollok(debug, TRUE);
}

void chip8::loadGame(char* filename) {
  FILE* game = fopen(filename, "rb");

  if (game != NULL) {
    fseek(game, 0, SEEK_END);
    unsigned int size = ftell(game);
    rewind(game);

    char* buffer = (char*) malloc(sizeof(char)* size);
    if (buffer != NULL) {
      if (fread(buffer, 1, size, game) == size) {
        for (unsigned int j = 0; j < size; j++) {
          mem[0x200 + j] = buffer[j];
        }
      }
    }

    fclose(game);
    free(buffer);
  }
}

void chip8::render() {
  wmove(win, 0, 0);
  for (int j = 0; j < 32; j++) {
    for (int k = 0; k < 64; k++) {
      waddch(win, gfx[k][j]);
    }
  }

  renderFlag = false;
  wrefresh(win);

  usleep(50000);
}

void chip8::emulateCycle() {
  unsigned short inst = (mem[pc] & 0xFF) << 8 | (mem[pc + 1] & 0xFF);

  nodelay(stdscr, TRUE);
  int ch = getch();
  if (ch != ERR) {
    wprintw(debug, "Key %d\n", ch);
    for (char j = 0; j < 10; j++) {
      if (ch == 48 + j) {
        key[j] = true;
      } else {
        key[j] = false;
      }
    }
    if (ch == 'a') {
      key[10] = true;
    } else if (ch == 'b') {
      key[11] = true;
    } else if (ch == 'c') {
      key[12] = true;
    } else if (ch == 'd') {
      key[13] = true;
    } else if (ch == 'e') {
      key[14] = true;
    } else if (ch == 'f') {
      key[15] = true;
    }

  } else {
    if (++missCount >= 100) {
      for (char j = 0; j < 16; j++) {
        key[j] = false;
      }
    }
  }
  nodelay(stdscr, FALSE);

  if (!decode_execute(inst)) {
    pc += 2;
  }

  wrefresh(debug);

  if (delay_timer != 0) {
    delay_timer--;
  }
  if (sound_timer != 0) {
    if (sound_timer == 1) {
      beep();
    }
    sound_timer--;
  }

  if (renderFlag) {
    chip8::render();
  }
}

bool chip8::decode_execute(unsigned short inst) {
  bool no_increment_pc = false;

  // X is always the second 4 bits, Y is always the 3rd
  const unsigned short X = (inst >> 8) & 0x000F;
  const unsigned short Y = (inst >> 4) & 0x000F;

  const unsigned short NNN = inst & 0x0FFF;
  const unsigned short NN = inst & 0x00FF;
  const unsigned short N = inst & 0x000F;

  unsigned char oldVX = v[X];

  // wprintw(debug, "Opcode: 0x%x\n", inst);
  switch (inst & 0xF000) {
    // Multiples
    case 0x0000:
      switch (inst & 0x00FF) {
        // Clear screen
        case 0xE0:
          for (int j = 0; j < 32; j++) {
            for (int k = 0; k < 64; k++) {
              gfx[k][j] = ' ';
            }
          }
          renderFlag = true;
          break;

          // Return from subroutine
        case 0xEE:
          pc = stack[(int)--sp];
          break;

          // TODO: OPCODE NNN
        default:
          std::cout << "RCA 1802 program not implemented\n";
          break;
      }
      break;

      // GOTO NNN
    case 0x1000:
      pc = NNN;
      no_increment_pc = true;
      break;

      // Call subroutine at NNN
    case 0x2000:
      stack[(int)sp++] = pc;
      pc = NNN;
      no_increment_pc = true;
      break;

      // If v[X] == NN, skip next inst
    case 0x3000:
      if (v[X] == NN) {
        pc += 2;
      }
      break;

      // If v[X] != NN, skip next inst
    case 0x4000:
      if (v[X] != NN) {
        pc += 2;
      }
      break;

      // If v[X] == v[Y], skip next inst
    case 0x5000:
      if (v[X] == v[Y]) {
        pc += 2;
      }
      break;

      // Sets v[X] to NN
    case 0x6000:
      v[X] = NN;
      break;

      // Add NN to v[X] (No carry flag)
    case 0x7000:
      v[X] += NN;
      break;

      // Multiples
    case 0x8000:
      switch (inst & 0x000F) {
        // Set v[X] to v[Y]
        case 0x0:
          v[X] = v[Y];
          break;

          // Set v[X] to v[X] OR v[Y]
        case 0x1:
          v[X] |= v[Y];
          break;

          // Set v[X] to v[X] AND v[Y]
        case 0x2:
          v[X] &= v[Y];
          break;

          // Set v[X] to v[X] XOR v[Y]
        case 0x3:
          v[X] ^= v[Y];
          break;

          // Add v[Y] to v[X] (Set carry flag)
        case 0x4:
          if ((int)v[X] + (int)v[Y] > 255) {
            v[0xF] = 1;
          } else {
            v[0xF] = 0;
          }

          v[X] += v[Y];
          break;

          // Subtract v[Y] from v[X] (Set borrow flag 0 when borrow occurs)
        case 0x5:
          if (v[X] < v[Y]) {
            v[0xF] = 0;
          } else {
            v[0xF] = 1;
          }

          v[X] -= v[Y];
          break;

          // Shift v[X] right by 1 and store the discarded bit in v[0xF]
        case 0x6:
          v[0xF] = v[X] & 1;
          v[X] >>= 1;
          break;

          // Subtract v[X] from v[Y] (Set borrow flag 0 when borrow occurs)
        case 0x7:
          if (v[Y] < v[X]) {
            v[0xF] = 0;
          } else {
            v[0xF] = 1;
          }

          v[X] = v[Y] - v[X];
          break;

          // Shift v[X] left by 1 and store the discarded bit in v[0xF]
        case 0xE:
          v[0xF] = (v[X] >> 15) & 1;
          v[X] <<= 1;
          break;
      }
      break;

      // Multiple
    case 0x9000:
      switch (inst & 0x000F) {
        // Skip next instruction if v[X] != v[Y]
        case 0x0:
          if (v[X] != v[Y]) {
            pc += 2;
          }
          break;

        default:
          wprintw(debug, "Invalid opcode 0x%x\n", inst);
          break;
      }
      break;

      // Set i to NNN
    case 0xA000:
      i = NNN;
      break;

      // Jump to address NNN + v[0]
    case 0xB000:
      pc = NNN + v[0];
      break;

      // Set v[X] to random # AND NN
    case 0xC000:
      v[X] = rand() & NN;
      break;

      // Draw sprite at v[X], v[Y] with width = 8 and height = N, via XOR
      // First row is found at memory address I
      // Set VF to 1 if any pixels go from true to false, otherwise 0
    case 0xD000:
      for (int j = 0; j < N; j++) {
        for (int k = 0; k < 8; k++) {
          unsigned short x = v[X] + k;
          unsigned short y = v[Y] + j;
          if (((mem[i + j] >> 7 - k) & 1) == 1) {
            if (gfx[x][y] == ' ') {
              gfx[x][y] = '#';
            } else {
              gfx[x][y] = ' ';
              v[0xF] = 1;
            }
          }
        }
      }
      renderFlag = true;
      break;

      // Multiple
    case 0xE000:
      switch (inst & 0x00FF) {
        // TODO: Skip next inst if key stored in v[X] is pressed
        case 0x9E:
          if (key[v[X]]) {
            pc += 2;
          }
          break;

          // TODO: Skip next inst if key stored in v[X] isn't pressed
        case 0xA1:
          if (key[v[X]]) {
            pc += 2;
          }
          break;

        default:
          wprintw(debug, "Invalid opcode 0x%x\n", inst);
          break;
      }
      break;

      // Multiple
    case 0xF000:
      switch (inst & 0x00FF) {
        // Set v[X] to value of delay timer
        case 0x07:
          v[X] = delay_timer;
          break;

          // TODO: Await key press, then store in v[X] (Blocking)
        case 0x0A:
          {
            int ch = getch();
            if (ch != ERR) {
              for (char j = 0; j < 10; j++) {
                if (ch == 48 + j) {
                  v[X] = j;
                  key[j] = true;
                } else {
                  key[j] = false;
                }
              }
              if (ch == 'a') {
                key[10] = true;
                v[X] = 10;
              } else if (ch == 'b') {
                key[11] = true;
                v[X] = 11;
              } else if (ch == 'c') {
                key[12] = true;
                v[X] = 12;
              } else if (ch == 'd') {
                key[13] = true;
                v[X] = 13;
              } else if (ch == 'e') {
                key[14] = true;
                v[X] = 14;
              } else if (ch == 'f') {
                key[15] = true;
                v[X] = 15;
              }
            }
            break;
          }

          // Set delay timer to v[X]
        case 0x15:
          delay_timer = v[X];
          break;

          // Set sound timer to v[X]
        case 0x18:
          sound_timer = v[X];
          break;

          // Add v[X] to I
        case 0x1E:
          i += v[X];
          break;

          // Sets I to the location of the sprite for the character in VX
          // Characters 0-F are represented by a 4x5 font
        case 0x29:
          i = 0x50 + 0x05 * (unsigned char)v[X];
          break;

          // Store binary-coded decimal representation of VX, with the most
          // significant of the three digits at the address in I, middle digit
          // I+1, least significant in I+2
        case 0x33:
          {
            unsigned char val = v[X];
            mem[i] = (char)(val / 100);
            val %= 100;
            mem[i + 1] = (char)(val / 10);
            mem[i + 2] = (char)(val % 10);
            break;
          }

          // Stores v[0] to v[X] in memory beginning at I
        case 0x55:
          for (unsigned short j = 0; j <= X; j++) {
            mem[i + j] = v[j];
          }
          break;

          // Fills v[0] to v[X] from memory beginning at I
        case 0x65:
          for (unsigned short j = 0; j <= X; j++) {
            v[j] = mem[i + j];
          }
          break;

        default:
          wprintw(debug, "Invalid opcode 0x%x\n", inst);
          break;
      }
      break;

    default:
      wprintw(debug, "Invalid opcode 0x%x\n", inst);
  }

  return no_increment_pc;
}
